'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = require('/Users/nandan/SkyDrive/Development/nextjs/nexthrone/node_modules/babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('/Users/nandan/SkyDrive/Development/nextjs/nexthrone/node_modules/babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('/Users/nandan/SkyDrive/Development/nextjs/nexthrone/node_modules/babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('/Users/nandan/SkyDrive/Development/nextjs/nexthrone/node_modules/babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('/Users/nandan/SkyDrive/Development/nextjs/nexthrone/node_modules/babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('/Users/nandan/SkyDrive/Development/nextjs/nexthrone/node_modules/react/react.js');

var _react2 = _interopRequireDefault(_react);

var _posts = require('../data/posts');

var _posts2 = _interopRequireDefault(_posts);

var _css = require('/Users/nandan/SkyDrive/Development/nextjs/nexthrone/node_modules/next/dist/lib/css.js');

var _lodash = require('lodash');

var _ = _interopRequireWildcard(_lodash);

var _AuthService = require('../utils/AuthService');

var _AuthService2 = _interopRequireDefault(_AuthService);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _class = function (_React$Component) {
  (0, _inherits3.default)(_class, _React$Component);

  function _class() {
    (0, _classCallCheck3.default)(this, _class);
    return (0, _possibleConstructorReturn3.default)(this, (_class.__proto__ || (0, _getPrototypeOf2.default)(_class)).apply(this, arguments));
  }

  (0, _createClass3.default)(_class, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.auth = new _AuthService2.default('juoHmPVzANigOzywGP6teeBH5hiBNaUi', 'calculi.eu.auth0.com');
      if (!this.auth.loggedIn()) {
        this.props.url.replaceTo('/');
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var item = _.find(_posts2.default, { id: this.props.url.query.id });

      return _react2.default.createElement(
        'div',
        { className: (0, _css.style)(styles.main) },
        _react2.default.createElement('script', { src: 'https://cdn.auth0.com/js/lock/10.5/lock.min.js' }),
        _react2.default.createElement(
          'div',
          { className: (0, _css.style)(styles.header) },
          _react2.default.createElement(
            'h3',
            null,
            ' NEXTHRONE - THE REVELATION OF GAME OF THRONES\' CHARACTERS '
          )
        ),
        _react2.default.createElement(
          'div',
          { className: (0, _css.style)(styles.panel) },
          _react2.default.createElement(
            'h1',
            { className: (0, _css.style)(styles.heading) },
            'Character: ',
            item.codeName,
            _react2.default.createElement('br', null),
            _react2.default.createElement('br', null),
            'Real Name: ',
            item.realName,
            _react2.default.createElement('br', null),
            _react2.default.createElement('br', null),
            'Brief Description:',
            _react2.default.createElement('br', null),
            _react2.default.createElement('br', null),
            _react2.default.createElement(
              'span',
              null,
              ' ',
              item.story,
              ' '
            )
          )
        ),
        _react2.default.createElement(
          'div',
          { className: (0, _css.style)(styles.singlePhoto) },
          _react2.default.createElement('img', { src: item.display_src, alt: item.realName, width: 500, height: 500 })
        )
      );
    }
  }]);
  return _class;
}(_react2.default.Component);

exports.default = _class;


var styles = {
  main: {
    padding: '50px'
  },

  header: {
    font: '15px Monaco',
    textAlign: 'center'
  },

  panel: {
    float: 'right',
    marginRight: '140px',
    width: '300px'
  },

  singlePhoto: {
    border: '1px solid #999',
    width: '500px',
    height: '500px',
    float: 'left'
  },

  heading: {
    font: '15px Monaco'
  }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBhZ2VzL2FjY291bnQuanMiXSwibmFtZXMiOlsiXyIsImF1dGgiLCJsb2dnZWRJbiIsInByb3BzIiwidXJsIiwicmVwbGFjZVRvIiwiaXRlbSIsImZpbmQiLCJpZCIsInF1ZXJ5Iiwic3R5bGVzIiwibWFpbiIsImhlYWRlciIsInBhbmVsIiwiaGVhZGluZyIsImNvZGVOYW1lIiwicmVhbE5hbWUiLCJzdG9yeSIsInNpbmdsZVBob3RvIiwiZGlzcGxheV9zcmMiLCJDb21wb25lbnQiLCJwYWRkaW5nIiwiZm9udCIsInRleHRBbGlnbiIsImZsb2F0IiwibWFyZ2luUmlnaHQiLCJ3aWR0aCIsImJvcmRlciIsImhlaWdodCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTs7OztBQUNBOzs7O0FBQ0E7O0FBQ0E7O0lBQVlBLEM7O0FBQ1o7Ozs7Ozs7Ozs7Ozs7Ozs7Ozt3Q0FHc0I7QUFDbEIsV0FBS0MsSUFBTCxHQUFZLDBCQUFnQixrQ0FBaEIsRUFBb0Qsc0JBQXBELENBQVo7QUFDQSxVQUFJLENBQUMsS0FBS0EsSUFBTCxDQUFVQyxRQUFWLEVBQUwsRUFBMkI7QUFDekIsYUFBS0MsS0FBTCxDQUFXQyxHQUFYLENBQWVDLFNBQWYsQ0FBeUIsR0FBekI7QUFDRDtBQUNGOzs7NkJBRVE7QUFDUCxVQUFNQyxPQUFPTixFQUFFTyxJQUFGLGtCQUFjLEVBQUNDLElBQUssS0FBS0wsS0FBTCxDQUFXQyxHQUFYLENBQWVLLEtBQWYsQ0FBcUJELEVBQTNCLEVBQWQsQ0FBYjs7QUFFQSxhQUNFO0FBQUE7QUFBQSxVQUFLLFdBQVcsZ0JBQU1FLE9BQU9DLElBQWIsQ0FBaEI7QUFDRSxrREFBUSxLQUFJLGdEQUFaLEdBREY7QUFFRTtBQUFBO0FBQUEsWUFBSyxXQUFXLGdCQUFNRCxPQUFPRSxNQUFiLENBQWhCO0FBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGLFNBRkY7QUFLRTtBQUFBO0FBQUEsWUFBSyxXQUFXLGdCQUFNRixPQUFPRyxLQUFiLENBQWhCO0FBQ0U7QUFBQTtBQUFBLGNBQUksV0FBVyxnQkFBTUgsT0FBT0ksT0FBYixDQUFmO0FBQUE7QUFDZVIsaUJBQUtTLFFBRHBCO0FBRUUscURBRkY7QUFHRSxxREFIRjtBQUFBO0FBSWVULGlCQUFLVSxRQUpwQjtBQUtFLHFEQUxGO0FBTUUscURBTkY7QUFBQTtBQVFFLHFEQVJGO0FBU0UscURBVEY7QUFVRTtBQUFBO0FBQUE7QUFBQTtBQUFTVixtQkFBS1csS0FBZDtBQUFBO0FBQUE7QUFWRjtBQURGLFNBTEY7QUFvQkU7QUFBQTtBQUFBLFlBQUssV0FBVyxnQkFBTVAsT0FBT1EsV0FBYixDQUFoQjtBQUNFLGlEQUFLLEtBQU1aLEtBQUthLFdBQWhCLEVBQTZCLEtBQUtiLEtBQUtVLFFBQXZDLEVBQWlELE9BQU8sR0FBeEQsRUFBNkQsUUFBUSxHQUFyRTtBQURGO0FBcEJGLE9BREY7QUEwQkQ7OztFQXJDMEIsZ0JBQU1JLFM7Ozs7O0FBeUNuQyxJQUFNVixTQUFTO0FBQ2JDLFFBQU07QUFDSlUsYUFBUztBQURMLEdBRE87O0FBS2JULFVBQVE7QUFDTlUsVUFBTSxhQURBO0FBRU5DLGVBQVc7QUFGTCxHQUxLOztBQVViVixTQUFPO0FBQ0xXLFdBQU8sT0FERjtBQUVMQyxpQkFBYSxPQUZSO0FBR0xDLFdBQU87QUFIRixHQVZNOztBQWdCYlIsZUFBYTtBQUNYUyxZQUFRLGdCQURHO0FBRVhELFdBQU8sT0FGSTtBQUdYRSxZQUFRLE9BSEc7QUFJWEosV0FBTztBQUpJLEdBaEJBOztBQXVCYlYsV0FBUztBQUNQUSxVQUFNO0FBREM7QUF2QkksQ0FBZiIsImZpbGUiOiJhY2NvdW50LmpzIiwic291cmNlUm9vdCI6Ii9Vc2Vycy9uYW5kYW4vU2t5RHJpdmUvRGV2ZWxvcG1lbnQvbmV4dGpzL25leHRocm9uZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCdcbmltcG9ydCBwb3N0cyBmcm9tICcuLi9kYXRhL3Bvc3RzJ1xuaW1wb3J0IHsgc3R5bGUgfSBmcm9tICduZXh0L2NzcydcbmltcG9ydCAqIGFzIF8gZnJvbSAnbG9kYXNoJ1xuaW1wb3J0IEF1dGhTZXJ2aWNlIGZyb20gJy4uL3V0aWxzL0F1dGhTZXJ2aWNlJ1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIHRoaXMuYXV0aCA9IG5ldyBBdXRoU2VydmljZSgnanVvSG1QVnpBTmlnT3p5d0dQNnRlZUJINWhpQk5hVWknLCAnY2FsY3VsaS5ldS5hdXRoMC5jb20nKTtcbiAgICBpZiAoIXRoaXMuYXV0aC5sb2dnZWRJbigpKSB7XG4gICAgICB0aGlzLnByb3BzLnVybC5yZXBsYWNlVG8oJy8nKVxuICAgIH1cbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCBpdGVtID0gXy5maW5kKHBvc3RzLCB7aWQgOiB0aGlzLnByb3BzLnVybC5xdWVyeS5pZH0pXG5cbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9e3N0eWxlKHN0eWxlcy5tYWluKX0+XG4gICAgICAgIDxzY3JpcHQgc3JjPVwiaHR0cHM6Ly9jZG4uYXV0aDAuY29tL2pzL2xvY2svMTAuNS9sb2NrLm1pbi5qc1wiPjwvc2NyaXB0PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17c3R5bGUoc3R5bGVzLmhlYWRlcil9PlxuICAgICAgICAgIDxoMz4gTkVYVEhST05FIC0gVEhFIFJFVkVMQVRJT04gT0YgR0FNRSBPRiBUSFJPTkVTJyBDSEFSQUNURVJTIDwvaDM+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17c3R5bGUoc3R5bGVzLnBhbmVsKX0+XG4gICAgICAgICAgPGgxIGNsYXNzTmFtZT17c3R5bGUoc3R5bGVzLmhlYWRpbmcpfT5cbiAgICAgICAgICAgIENoYXJhY3RlcjogeyBpdGVtLmNvZGVOYW1lIH1cbiAgICAgICAgICAgIDxici8+XG4gICAgICAgICAgICA8YnIvPlxuICAgICAgICAgICAgUmVhbCBOYW1lOiB7IGl0ZW0ucmVhbE5hbWUgfVxuICAgICAgICAgICAgPGJyLz5cbiAgICAgICAgICAgIDxici8+XG4gICAgICAgICAgICBCcmllZiBEZXNjcmlwdGlvbjpcbiAgICAgICAgICAgIDxici8+XG4gICAgICAgICAgICA8YnIvPlxuICAgICAgICAgICAgPHNwYW4+IHsgaXRlbS5zdG9yeSB9IDwvc3Bhbj5cbiAgICAgICAgICA8L2gxPlxuICAgICAgICA8L2Rpdj5cblxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17c3R5bGUoc3R5bGVzLnNpbmdsZVBob3RvKX0+XG4gICAgICAgICAgPGltZyBzcmM9eyBpdGVtLmRpc3BsYXlfc3JjfSBhbHQ9e2l0ZW0ucmVhbE5hbWV9IHdpZHRoPXs1MDB9IGhlaWdodD17NTAwfSAvPlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvZGl2PlxuICAgIClcbiAgfVxufVxuXG5cbmNvbnN0IHN0eWxlcyA9IHtcbiAgbWFpbjoge1xuICAgIHBhZGRpbmc6ICc1MHB4J1xuICB9LFxuXG4gIGhlYWRlcjoge1xuICAgIGZvbnQ6ICcxNXB4IE1vbmFjbycsXG4gICAgdGV4dEFsaWduOiAnY2VudGVyJ1xuICB9LFxuXG4gIHBhbmVsOiB7XG4gICAgZmxvYXQ6ICdyaWdodCcsXG4gICAgbWFyZ2luUmlnaHQ6ICcxNDBweCcsXG4gICAgd2lkdGg6ICczMDBweCdcbiAgfSxcblxuICBzaW5nbGVQaG90bzoge1xuICAgIGJvcmRlcjogJzFweCBzb2xpZCAjOTk5JyxcbiAgICB3aWR0aDogJzUwMHB4JyxcbiAgICBoZWlnaHQ6ICc1MDBweCcsXG4gICAgZmxvYXQ6ICdsZWZ0J1xuICB9LFxuXG4gIGhlYWRpbmc6IHtcbiAgICBmb250OiAnMTVweCBNb25hY28nXG4gIH1cbn1cbiJdfQ==