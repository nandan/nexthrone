'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = require('/Users/nandan/SkyDrive/Development/nextjs/nexthrone/node_modules/babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('/Users/nandan/SkyDrive/Development/nextjs/nexthrone/node_modules/babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _possibleConstructorReturn2 = require('/Users/nandan/SkyDrive/Development/nextjs/nexthrone/node_modules/babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _createClass2 = require('/Users/nandan/SkyDrive/Development/nextjs/nexthrone/node_modules/babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _inherits2 = require('/Users/nandan/SkyDrive/Development/nextjs/nexthrone/node_modules/babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('/Users/nandan/SkyDrive/Development/nextjs/nexthrone/node_modules/react/react.js');

var _react2 = _interopRequireDefault(_react);

var _posts = require('../data/posts');

var _posts2 = _interopRequireDefault(_posts);

var _css = require('/Users/nandan/SkyDrive/Development/nextjs/nexthrone/node_modules/next/dist/lib/css.js');

var _link = require('/Users/nandan/SkyDrive/Development/nextjs/nexthrone/node_modules/next/dist/lib/link.js');

var _link2 = _interopRequireDefault(_link);

var _AuthService = require('../utils/AuthService');

var _AuthService2 = _interopRequireDefault(_AuthService);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var main = function (_React$Component) {
  (0, _inherits3.default)(main, _React$Component);
  (0, _createClass3.default)(main, null, [{
    key: 'getInitialProps',
    value: function getInitialProps() {
      return { posts: _posts2.default };
    }
  }]);

  function main(props) {
    (0, _classCallCheck3.default)(this, main);

    var _this = (0, _possibleConstructorReturn3.default)(this, (main.__proto__ || (0, _getPrototypeOf2.default)(main)).call(this, props));

    _this.state = { loggedIn: false };
    return _this;
  }

  (0, _createClass3.default)(main, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var _this2 = this;

      this.auth = new _AuthService2.default('juoHmPVzANigOzywGP6teeBH5hiBNaUi', 'calculi.eu.auth0.com');
      this.setState({ loggedIn: this.auth.loggedIn() });
      // instance of Lock
      this.lock = this.auth.getLock();
      this.lock.on('authenticated', function () {
        _this2.setState({ loggedIn: _this2.auth.loggedIn() });
      });
    }
  }, {
    key: 'login',
    value: function login() {
      this.auth.login();
    }
  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      var loginButton = this.state.loggedIn ? _react2.default.createElement(
        'div',
        null,
        'HELLO'
      ) : _react2.default.createElement(
        'button',
        { onClick: this.login.bind(this) },
        'Login'
      );

      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          'div',
          { className: (0, _css.style)(styles.header) },
          _react2.default.createElement('script', { src: 'https://cdn.auth0.com/js/lock/10.5/lock.min.js' }),
          loginButton,
          _react2.default.createElement(
            'h3',
            null,
            'NexThrone - The Revelation of Game of Thrones\' Characters'
          )
        ),
        _react2.default.createElement(
          'table',
          { className: (0, _css.style)(styles.table) },
          _react2.default.createElement(
            'thead',
            null,
            _react2.default.createElement(
              'tr',
              null,
              _react2.default.createElement(
                'th',
                { className: (0, _css.style)(styles.th) },
                'Character'
              ),
              _react2.default.createElement(
                'th',
                { className: (0, _css.style)(styles.th) },
                'Real Name'
              )
            )
          ),
          _react2.default.createElement(
            'tbody',
            null,
            this.props.posts.map(function (post, i) {
              return _react2.default.createElement(
                'tr',
                { key: i },
                _react2.default.createElement(
                  'td',
                  { className: (0, _css.style)(styles.td) },
                  post.codeName
                ),
                _react2.default.createElement(
                  'td',
                  { className: (0, _css.style)(styles.td) },
                  _this3.state.loggedIn ? _react2.default.createElement(
                    _link2.default,
                    { href: '/account?id=' + post.id },
                    post.realName
                  ) : _react2.default.createElement(
                    'div',
                    null,
                    'You need to login'
                  )
                )
              );
            })
          )
        )
      );
    }
  }]);
  return main;
}(_react2.default.Component);

var styles = {
  th: {
    background: '#00cccc',
    color: '#fff',
    textTransform: 'uppercase',
    fontSize: '12px',
    padding: '12px 35px'
  },

  header: {
    font: '15px Monaco',
    textAlign: 'center'
  },

  table: {
    fontFamily: 'Arial',
    margin: '25px auto',
    borderCollapse: 'collapse',
    border: '1px solid #eee',
    borderBottom: '2px solid #00cccc'
  },

  td: {
    color: '#999',
    border: '1px solid #eee',
    padding: '12px 35px',
    borderCollapse: 'collapse'
  },

  list: {
    padding: '50px',
    textAlign: 'center'
  },

  photo: {
    display: 'inline-block'
  },

  photoLink: {
    color: '#333',
    verticalAlign: 'middle',
    cursor: 'pointer',
    background: '#eee',
    display: 'inline-block',
    width: '250px',
    height: '250px',
    lineHeight: '250px',
    margin: '10px',
    border: '2px solid transparent',
    ':hover': {
      borderColor: 'blue'
    }
  }
};

exports.default = main;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBhZ2VzL2luZGV4LmpzIl0sIm5hbWVzIjpbIm1haW4iLCJwb3N0cyIsInByb3BzIiwic3RhdGUiLCJsb2dnZWRJbiIsImF1dGgiLCJzZXRTdGF0ZSIsImxvY2siLCJnZXRMb2NrIiwib24iLCJsb2dpbiIsImxvZ2luQnV0dG9uIiwiYmluZCIsInN0eWxlcyIsImhlYWRlciIsInRhYmxlIiwidGgiLCJtYXAiLCJwb3N0IiwiaSIsInRkIiwiY29kZU5hbWUiLCJpZCIsInJlYWxOYW1lIiwiQ29tcG9uZW50IiwiYmFja2dyb3VuZCIsImNvbG9yIiwidGV4dFRyYW5zZm9ybSIsImZvbnRTaXplIiwicGFkZGluZyIsImZvbnQiLCJ0ZXh0QWxpZ24iLCJmb250RmFtaWx5IiwibWFyZ2luIiwiYm9yZGVyQ29sbGFwc2UiLCJib3JkZXIiLCJib3JkZXJCb3R0b20iLCJsaXN0IiwicGhvdG8iLCJkaXNwbGF5IiwicGhvdG9MaW5rIiwidmVydGljYWxBbGlnbiIsImN1cnNvciIsIndpZHRoIiwiaGVpZ2h0IiwibGluZUhlaWdodCIsImJvcmRlckNvbG9yIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBOzs7O0FBQ0E7Ozs7QUFDQTs7QUFDQTs7OztBQUNBOzs7Ozs7SUFFTUEsSTs7OztzQ0FDcUI7QUFDdkIsYUFBTyxFQUFFQyxzQkFBRixFQUFQO0FBQ0Q7OztBQUVELGdCQUFZQyxLQUFaLEVBQW1CO0FBQUE7O0FBQUEsa0lBQ1hBLEtBRFc7O0FBRWpCLFVBQUtDLEtBQUwsR0FBYSxFQUFFQyxVQUFVLEtBQVosRUFBYjtBQUZpQjtBQUdsQjs7Ozt3Q0FFbUI7QUFBQTs7QUFDbEIsV0FBS0MsSUFBTCxHQUFZLDBCQUFnQixrQ0FBaEIsRUFBb0Qsc0JBQXBELENBQVo7QUFDQSxXQUFLQyxRQUFMLENBQWMsRUFBRUYsVUFBVSxLQUFLQyxJQUFMLENBQVVELFFBQVYsRUFBWixFQUFkO0FBQ0E7QUFDQSxXQUFLRyxJQUFMLEdBQVksS0FBS0YsSUFBTCxDQUFVRyxPQUFWLEVBQVo7QUFDQSxXQUFLRCxJQUFMLENBQVVFLEVBQVYsQ0FBYSxlQUFiLEVBQThCLFlBQU07QUFDbEMsZUFBS0gsUUFBTCxDQUFjLEVBQUVGLFVBQVUsT0FBS0MsSUFBTCxDQUFVRCxRQUFWLEVBQVosRUFBZDtBQUNELE9BRkQ7QUFHRDs7OzRCQUVPO0FBQ04sV0FBS0MsSUFBTCxDQUFVSyxLQUFWO0FBQ0Q7Ozs2QkFFUTtBQUFBOztBQUNQLFVBQU1DLGNBQWMsS0FBS1IsS0FBTCxDQUFXQyxRQUFYLEdBQXNCO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FBdEIsR0FBeUM7QUFBQTtBQUFBLFVBQVEsU0FBUyxLQUFLTSxLQUFMLENBQVdFLElBQVgsQ0FBZ0IsSUFBaEIsQ0FBakI7QUFBQTtBQUFBLE9BQTdEOztBQUVBLGFBQ0U7QUFBQTtBQUFBO0FBQ0U7QUFBQTtBQUFBLFlBQUssV0FBVyxnQkFBTUMsT0FBT0MsTUFBYixDQUFoQjtBQUNFLG9EQUFRLEtBQUksZ0RBQVosR0FERjtBQUVJSCxxQkFGSjtBQUdFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFIRixTQURGO0FBTUU7QUFBQTtBQUFBLFlBQU8sV0FBVyxnQkFBTUUsT0FBT0UsS0FBYixDQUFsQjtBQUNFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQTtBQUNJO0FBQUE7QUFBQSxrQkFBSSxXQUFXLGdCQUFNRixPQUFPRyxFQUFiLENBQWY7QUFBQTtBQUFBLGVBREo7QUFFSTtBQUFBO0FBQUEsa0JBQUksV0FBVyxnQkFBTUgsT0FBT0csRUFBYixDQUFmO0FBQUE7QUFBQTtBQUZKO0FBREYsV0FERjtBQU9FO0FBQUE7QUFBQTtBQUVJLGlCQUFLZCxLQUFMLENBQVdELEtBQVgsQ0FBaUJnQixHQUFqQixDQUFzQixVQUFDQyxJQUFELEVBQU9DLENBQVA7QUFBQSxxQkFDcEI7QUFBQTtBQUFBLGtCQUFJLEtBQUtBLENBQVQ7QUFDRTtBQUFBO0FBQUEsb0JBQUksV0FBVyxnQkFBTU4sT0FBT08sRUFBYixDQUFmO0FBQW1DRix1QkFBS0c7QUFBeEMsaUJBREY7QUFFRTtBQUFBO0FBQUEsb0JBQUksV0FBVyxnQkFBTVIsT0FBT08sRUFBYixDQUFmO0FBQ0kseUJBQUtqQixLQUFMLENBQVdDLFFBQVgsR0FBc0I7QUFBQTtBQUFBLHNCQUFNLHVCQUFxQmMsS0FBS0ksRUFBaEM7QUFBd0NKLHlCQUFLSztBQUE3QyxtQkFBdEIsR0FBdUY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUQzRjtBQUZGLGVBRG9CO0FBQUEsYUFBdEI7QUFGSjtBQVBGO0FBTkYsT0FERjtBQTZCRDs7O0VBeERnQixnQkFBTUMsUzs7QUEyRHpCLElBQU1YLFNBQVM7QUFDYkcsTUFBSTtBQUNGUyxnQkFBWSxTQURWO0FBRUZDLFdBQU8sTUFGTDtBQUdGQyxtQkFBZSxXQUhiO0FBSUZDLGNBQVUsTUFKUjtBQUtGQyxhQUFTO0FBTFAsR0FEUzs7QUFTYmYsVUFBUTtBQUNOZ0IsVUFBTSxhQURBO0FBRU5DLGVBQVc7QUFGTCxHQVRLOztBQWNiaEIsU0FBTztBQUNMaUIsZ0JBQVksT0FEUDtBQUVMQyxZQUFRLFdBRkg7QUFHTEMsb0JBQWdCLFVBSFg7QUFJTEMsWUFBUSxnQkFKSDtBQUtMQyxrQkFBYztBQUxULEdBZE07O0FBc0JiaEIsTUFBSTtBQUNGTSxXQUFPLE1BREw7QUFFRlMsWUFBUSxnQkFGTjtBQUdGTixhQUFTLFdBSFA7QUFJRkssb0JBQWdCO0FBSmQsR0F0QlM7O0FBNkJiRyxRQUFNO0FBQ0pSLGFBQVMsTUFETDtBQUVKRSxlQUFXO0FBRlAsR0E3Qk87O0FBa0NiTyxTQUFPO0FBQ0xDLGFBQVM7QUFESixHQWxDTTs7QUFzQ2JDLGFBQVc7QUFDVGQsV0FBTyxNQURFO0FBRVRlLG1CQUFlLFFBRk47QUFHVEMsWUFBUSxTQUhDO0FBSVRqQixnQkFBWSxNQUpIO0FBS1RjLGFBQVMsY0FMQTtBQU1USSxXQUFPLE9BTkU7QUFPVEMsWUFBUSxPQVBDO0FBUVRDLGdCQUFZLE9BUkg7QUFTVFosWUFBUSxNQVRDO0FBVVRFLFlBQVEsdUJBVkM7QUFXVCxjQUFVO0FBQ1JXLG1CQUFhO0FBREw7QUFYRDtBQXRDRSxDQUFmOztrQkF1RGU5QyxJIiwiZmlsZSI6ImluZGV4LmpzIiwic291cmNlUm9vdCI6Ii9Vc2Vycy9uYW5kYW4vU2t5RHJpdmUvRGV2ZWxvcG1lbnQvbmV4dGpzL25leHRocm9uZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCdcbmltcG9ydCBwb3N0cyBmcm9tICcuLi9kYXRhL3Bvc3RzJ1xuaW1wb3J0IHsgc3R5bGUgfSBmcm9tICduZXh0L2NzcydcbmltcG9ydCBMaW5rIGZyb20gJ25leHQvbGluaydcbmltcG9ydCBBdXRoU2VydmljZSBmcm9tICcuLi91dGlscy9BdXRoU2VydmljZSdcblxuY2xhc3MgbWFpbiBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gIHN0YXRpYyBnZXRJbml0aWFsUHJvcHMoKSB7XG4gICAgcmV0dXJuIHsgcG9zdHM6IHBvc3RzIH1cbiAgfVxuXG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpXG4gICAgdGhpcy5zdGF0ZSA9IHsgbG9nZ2VkSW46IGZhbHNlIH1cbiAgfVxuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIHRoaXMuYXV0aCA9IG5ldyBBdXRoU2VydmljZSgnanVvSG1QVnpBTmlnT3p5d0dQNnRlZUJINWhpQk5hVWknLCAnY2FsY3VsaS5ldS5hdXRoMC5jb20nKTtcbiAgICB0aGlzLnNldFN0YXRlKHsgbG9nZ2VkSW46IHRoaXMuYXV0aC5sb2dnZWRJbigpIH0pXG4gICAgLy8gaW5zdGFuY2Ugb2YgTG9ja1xuICAgIHRoaXMubG9jayA9IHRoaXMuYXV0aC5nZXRMb2NrKCk7XG4gICAgdGhpcy5sb2NrLm9uKCdhdXRoZW50aWNhdGVkJywgKCkgPT4ge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7IGxvZ2dlZEluOiB0aGlzLmF1dGgubG9nZ2VkSW4oKSB9KVxuICAgIH0pO1xuICB9XG5cbiAgbG9naW4oKSB7XG4gICAgdGhpcy5hdXRoLmxvZ2luKCk7XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgbG9naW5CdXR0b24gPSB0aGlzLnN0YXRlLmxvZ2dlZEluID8gPGRpdj5IRUxMTzwvZGl2PiA6IDxidXR0b24gb25DbGljaz17dGhpcy5sb2dpbi5iaW5kKHRoaXMpfT5Mb2dpbjwvYnV0dG9uPjtcblxuICAgIHJldHVybihcbiAgICAgIDxkaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzdHlsZShzdHlsZXMuaGVhZGVyKX0+XG4gICAgICAgICAgPHNjcmlwdCBzcmM9XCJodHRwczovL2Nkbi5hdXRoMC5jb20vanMvbG9jay8xMC41L2xvY2subWluLmpzXCI+PC9zY3JpcHQ+XG4gICAgICAgICAgeyBsb2dpbkJ1dHRvbiB9XG4gICAgICAgICAgPGgzPk5leFRocm9uZSAtIFRoZSBSZXZlbGF0aW9uIG9mIEdhbWUgb2YgVGhyb25lcycgQ2hhcmFjdGVyczwvaDM+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8dGFibGUgY2xhc3NOYW1lPXtzdHlsZShzdHlsZXMudGFibGUpfT5cbiAgICAgICAgICA8dGhlYWQ+XG4gICAgICAgICAgICA8dHI+XG4gICAgICAgICAgICAgICAgPHRoIGNsYXNzTmFtZT17c3R5bGUoc3R5bGVzLnRoKX0+Q2hhcmFjdGVyPC90aD5cbiAgICAgICAgICAgICAgICA8dGggY2xhc3NOYW1lPXtzdHlsZShzdHlsZXMudGgpfT5SZWFsIE5hbWU8L3RoPlxuICAgICAgICAgICAgPC90cj5cbiAgICAgICAgICA8L3RoZWFkPlxuICAgICAgICAgIDx0Ym9keT5cbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgdGhpcy5wcm9wcy5wb3N0cy5tYXAoIChwb3N0LCBpKSA9PiAoXG4gICAgICAgICAgICAgICAgPHRyIGtleT17aX0+XG4gICAgICAgICAgICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZShzdHlsZXMudGQpfT57IHBvc3QuY29kZU5hbWUgfTwvdGQ+XG4gICAgICAgICAgICAgICAgICA8dGQgY2xhc3NOYW1lPXtzdHlsZShzdHlsZXMudGQpfT5cbiAgICAgICAgICAgICAgICAgICAgeyB0aGlzLnN0YXRlLmxvZ2dlZEluID8gPExpbmsgaHJlZj17YC9hY2NvdW50P2lkPSR7cG9zdC5pZH1gfT57IHBvc3QucmVhbE5hbWUgfTwvTGluaz4gOiA8ZGl2PllvdSBuZWVkIHRvIGxvZ2luPC9kaXY+IH1cbiAgICAgICAgICAgICAgICAgIDwvdGQ+XG4gICAgICAgICAgICAgICAgPC90cj5cbiAgICAgICAgICAgICAgKSlcbiAgICAgICAgICAgIH1cbiAgICAgICAgICA8L3Rib2R5PlxuICAgICAgICA8L3RhYmxlPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5jb25zdCBzdHlsZXMgPSB7XG4gIHRoOiB7XG4gICAgYmFja2dyb3VuZDogJyMwMGNjY2MnLFxuICAgIGNvbG9yOiAnI2ZmZicsXG4gICAgdGV4dFRyYW5zZm9ybTogJ3VwcGVyY2FzZScsXG4gICAgZm9udFNpemU6ICcxMnB4JyxcbiAgICBwYWRkaW5nOiAnMTJweCAzNXB4JyxcbiAgfSxcblxuICBoZWFkZXI6IHtcbiAgICBmb250OiAnMTVweCBNb25hY28nLFxuICAgIHRleHRBbGlnbjogJ2NlbnRlcidcbiAgfSxcblxuICB0YWJsZToge1xuICAgIGZvbnRGYW1pbHk6ICdBcmlhbCcsXG4gICAgbWFyZ2luOiAnMjVweCBhdXRvJyxcbiAgICBib3JkZXJDb2xsYXBzZTogJ2NvbGxhcHNlJyxcbiAgICBib3JkZXI6ICcxcHggc29saWQgI2VlZScsXG4gICAgYm9yZGVyQm90dG9tOiAnMnB4IHNvbGlkICMwMGNjY2MnXG4gIH0sXG5cbiAgdGQ6IHtcbiAgICBjb2xvcjogJyM5OTknLFxuICAgIGJvcmRlcjogJzFweCBzb2xpZCAjZWVlJyxcbiAgICBwYWRkaW5nOiAnMTJweCAzNXB4JyxcbiAgICBib3JkZXJDb2xsYXBzZTogJ2NvbGxhcHNlJ1xuICB9LFxuXG4gIGxpc3Q6IHtcbiAgICBwYWRkaW5nOiAnNTBweCcsXG4gICAgdGV4dEFsaWduOiAnY2VudGVyJ1xuICB9LFxuXG4gIHBob3RvOiB7XG4gICAgZGlzcGxheTogJ2lubGluZS1ibG9jaydcbiAgfSxcblxuICBwaG90b0xpbms6IHtcbiAgICBjb2xvcjogJyMzMzMnLFxuICAgIHZlcnRpY2FsQWxpZ246ICdtaWRkbGUnLFxuICAgIGN1cnNvcjogJ3BvaW50ZXInLFxuICAgIGJhY2tncm91bmQ6ICcjZWVlJyxcbiAgICBkaXNwbGF5OiAnaW5saW5lLWJsb2NrJyxcbiAgICB3aWR0aDogJzI1MHB4JyxcbiAgICBoZWlnaHQ6ICcyNTBweCcsXG4gICAgbGluZUhlaWdodDogJzI1MHB4JyxcbiAgICBtYXJnaW46ICcxMHB4JyxcbiAgICBib3JkZXI6ICcycHggc29saWQgdHJhbnNwYXJlbnQnLFxuICAgICc6aG92ZXInOiB7XG4gICAgICBib3JkZXJDb2xvcjogJ2JsdWUnXG4gICAgfVxuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IG1haW5cbiJdfQ==