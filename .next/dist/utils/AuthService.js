'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _classCallCheck2 = require('/Users/nandan/SkyDrive/Development/nextjs/nexthrone/node_modules/babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('/Users/nandan/SkyDrive/Development/nextjs/nexthrone/node_modules/babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _react = require('/Users/nandan/SkyDrive/Development/nextjs/nexthrone/node_modules/react/react.js');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AuthService = function () {
  function AuthService(clientId, domain) {
    (0, _classCallCheck3.default)(this, AuthService);

    //configure Auth0
    this.clientId = clientId;
    this.domain = domain;

    this.lock = new Auth0Lock(clientId, domain, {});
    // Add callback for lock `authenticated` event
    this.lock.on('authenticated', this._doAuthentication.bind(this));
    // binds login functions to keep this context
    this.login = this.login.bind(this);
  }

  (0, _createClass3.default)(AuthService, [{
    key: '_doAuthentication',
    value: function _doAuthentication(authResult) {
      // Saves the user token
      this.setToken(authResult.idToken);
    }
  }, {
    key: 'getLock',
    value: function getLock() {
      // An instance of Lock
      return new Auth0Lock(this.clientId, this.domain, {});
    }
  }, {
    key: 'login',
    value: function login() {
      // Call the show method to display the widget
      this.lock.show();
    }
  }, {
    key: 'loggedIn',
    value: function loggedIn() {
      // Checks if there is a saved token and it is still valid
      return !!this.getToken();
    }
  }, {
    key: 'setToken',
    value: function setToken(idToken) {
      // Saves user token to localStorage
      localStorage.setItem('id_token', idToken);
    }
  }, {
    key: 'getToken',
    value: function getToken() {
      // Retrieves the user token from localStorage
      return localStorage.getItem('id_token');
    }
  }, {
    key: 'logout',
    value: function logout() {
      // Clear user token and profile data from localStorage
      localStorage.removeItem('id_token');
    }
  }]);
  return AuthService;
}();

exports.default = AuthService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInV0aWxzL0F1dGhTZXJ2aWNlLmpzIl0sIm5hbWVzIjpbIkF1dGhTZXJ2aWNlIiwiY2xpZW50SWQiLCJkb21haW4iLCJsb2NrIiwiQXV0aDBMb2NrIiwib24iLCJfZG9BdXRoZW50aWNhdGlvbiIsImJpbmQiLCJsb2dpbiIsImF1dGhSZXN1bHQiLCJzZXRUb2tlbiIsImlkVG9rZW4iLCJzaG93IiwiZ2V0VG9rZW4iLCJsb2NhbFN0b3JhZ2UiLCJzZXRJdGVtIiwiZ2V0SXRlbSIsInJlbW92ZUl0ZW0iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7O0FBQUE7Ozs7OztJQUVxQkEsVztBQUNuQix1QkFBWUMsUUFBWixFQUFzQkMsTUFBdEIsRUFBOEI7QUFBQTs7QUFDNUI7QUFDQSxTQUFLRCxRQUFMLEdBQWdCQSxRQUFoQjtBQUNBLFNBQUtDLE1BQUwsR0FBY0EsTUFBZDs7QUFFQSxTQUFLQyxJQUFMLEdBQVksSUFBSUMsU0FBSixDQUFjSCxRQUFkLEVBQXdCQyxNQUF4QixFQUFnQyxFQUFoQyxDQUFaO0FBQ0E7QUFDQSxTQUFLQyxJQUFMLENBQVVFLEVBQVYsQ0FBYSxlQUFiLEVBQThCLEtBQUtDLGlCQUFMLENBQXVCQyxJQUF2QixDQUE0QixJQUE1QixDQUE5QjtBQUNBO0FBQ0EsU0FBS0MsS0FBTCxHQUFhLEtBQUtBLEtBQUwsQ0FBV0QsSUFBWCxDQUFnQixJQUFoQixDQUFiO0FBQ0Q7Ozs7c0NBRWlCRSxVLEVBQVc7QUFDM0I7QUFDQSxXQUFLQyxRQUFMLENBQWNELFdBQVdFLE9BQXpCO0FBQ0Q7Ozs4QkFFUztBQUNSO0FBQ0EsYUFBTyxJQUFJUCxTQUFKLENBQWMsS0FBS0gsUUFBbkIsRUFBNkIsS0FBS0MsTUFBbEMsRUFBMEMsRUFBMUMsQ0FBUDtBQUNEOzs7NEJBRU87QUFDTjtBQUNBLFdBQUtDLElBQUwsQ0FBVVMsSUFBVjtBQUNEOzs7K0JBRVU7QUFDVDtBQUNBLGFBQU8sQ0FBQyxDQUFDLEtBQUtDLFFBQUwsRUFBVDtBQUNEOzs7NkJBRVFGLE8sRUFBUTtBQUNmO0FBQ0FHLG1CQUFhQyxPQUFiLENBQXFCLFVBQXJCLEVBQWlDSixPQUFqQztBQUNEOzs7K0JBRVM7QUFDUjtBQUNBLGFBQU9HLGFBQWFFLE9BQWIsQ0FBcUIsVUFBckIsQ0FBUDtBQUNEOzs7NkJBRU87QUFDTjtBQUNBRixtQkFBYUcsVUFBYixDQUF3QixVQUF4QjtBQUNEOzs7OztrQkE5Q2tCakIsVyIsImZpbGUiOiJBdXRoU2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIvVXNlcnMvbmFuZGFuL1NreURyaXZlL0RldmVsb3BtZW50L25leHRqcy9uZXh0aHJvbmUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnXG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEF1dGhTZXJ2aWNlIHtcbiAgY29uc3RydWN0b3IoY2xpZW50SWQsIGRvbWFpbikge1xuICAgIC8vY29uZmlndXJlIEF1dGgwXG4gICAgdGhpcy5jbGllbnRJZCA9IGNsaWVudElkXG4gICAgdGhpcy5kb21haW4gPSBkb21haW5cblxuICAgIHRoaXMubG9jayA9IG5ldyBBdXRoMExvY2soY2xpZW50SWQsIGRvbWFpbiwge30pXG4gICAgLy8gQWRkIGNhbGxiYWNrIGZvciBsb2NrIGBhdXRoZW50aWNhdGVkYCBldmVudFxuICAgIHRoaXMubG9jay5vbignYXV0aGVudGljYXRlZCcsIHRoaXMuX2RvQXV0aGVudGljYXRpb24uYmluZCh0aGlzKSlcbiAgICAvLyBiaW5kcyBsb2dpbiBmdW5jdGlvbnMgdG8ga2VlcCB0aGlzIGNvbnRleHRcbiAgICB0aGlzLmxvZ2luID0gdGhpcy5sb2dpbi5iaW5kKHRoaXMpXG4gIH1cblxuICBfZG9BdXRoZW50aWNhdGlvbihhdXRoUmVzdWx0KXtcbiAgICAvLyBTYXZlcyB0aGUgdXNlciB0b2tlblxuICAgIHRoaXMuc2V0VG9rZW4oYXV0aFJlc3VsdC5pZFRva2VuKVxuICB9XG5cbiAgZ2V0TG9jaygpIHtcbiAgICAvLyBBbiBpbnN0YW5jZSBvZiBMb2NrXG4gICAgcmV0dXJuIG5ldyBBdXRoMExvY2sodGhpcy5jbGllbnRJZCwgdGhpcy5kb21haW4sIHt9KTtcbiAgfVxuXG4gIGxvZ2luKCkge1xuICAgIC8vIENhbGwgdGhlIHNob3cgbWV0aG9kIHRvIGRpc3BsYXkgdGhlIHdpZGdldFxuICAgIHRoaXMubG9jay5zaG93KClcbiAgfVxuXG4gIGxvZ2dlZEluKCkge1xuICAgIC8vIENoZWNrcyBpZiB0aGVyZSBpcyBhIHNhdmVkIHRva2VuIGFuZCBpdCBpcyBzdGlsbCB2YWxpZFxuICAgIHJldHVybiAhIXRoaXMuZ2V0VG9rZW4oKTtcbiAgfVxuXG4gIHNldFRva2VuKGlkVG9rZW4pe1xuICAgIC8vIFNhdmVzIHVzZXIgdG9rZW4gdG8gbG9jYWxTdG9yYWdlXG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ2lkX3Rva2VuJywgaWRUb2tlbilcbiAgfVxuXG4gIGdldFRva2VuKCl7XG4gICAgLy8gUmV0cmlldmVzIHRoZSB1c2VyIHRva2VuIGZyb20gbG9jYWxTdG9yYWdlXG4gICAgcmV0dXJuIGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdpZF90b2tlbicpXG4gIH1cblxuICBsb2dvdXQoKXtcbiAgICAvLyBDbGVhciB1c2VyIHRva2VuIGFuZCBwcm9maWxlIGRhdGEgZnJvbSBsb2NhbFN0b3JhZ2VcbiAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSgnaWRfdG9rZW4nKTtcbiAgfVxufVxuIl19